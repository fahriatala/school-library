package com.shared.library;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
@Configuration
@ComponentScan("com.shared.library")
public class GlobalExceptionHandler {
    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorResponse> handleException(Exception ex) {
        HttpStatus status;
        ErrorResponse errorResponse;
        if (ex instanceof DataNotFoundException) {
            errorResponse = new ErrorResponse("Data Not Found", ex.getMessage());
            status = HttpStatus.NOT_FOUND;
        } else if (ex instanceof AppException) {
            errorResponse = new ErrorResponse("Bad Request", ex.getMessage());
            status = HttpStatus.BAD_REQUEST;
        } else {
            errorResponse = new ErrorResponse("Internal Server Error", "An unexpected error occurred.");
            status = HttpStatus.INTERNAL_SERVER_ERROR;
        }

        return new ResponseEntity<>(errorResponse, status);
    }
}
